#!/usr/bin/env bash
mkdir .public
cd src
bundler exec jekyll build --config _config.yml
cp -aRf _site/* ../.public
cd ..
sudo chmod -R 777  *
