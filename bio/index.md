---
layout: default
title: Bio
rank: 2
---

<div class="starter-template">
    <h1>Bio</h1>
</div>

<div class="row">
  <h2><span class="text-primary">Positions</span></h2>
  {% include print_bio.html items=site.data.bio.position %}
</div>

<div class="row">
  <h2><span class="text-primary">International collaborations</span></h2>
  {% include print_bio.html items=site.data.bio.international %}
</div>

<div class="row">
  <h2><span class="text-primary">Education</span></h2>
  {% include print_bio.html items=site.data.bio.education %}
</div>
